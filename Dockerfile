FROM golang:alpine as builder

WORKDIR /app

COPY ./hello_serv.go . 

RUN go mod init serv

RUN go mod download serv

RUN go build -o /app/build


FROM scratch

WORKDIR /build

COPY --from=builder /app/build /build

ENTRYPOINT [ "./build" ]
